#include <sys/syscall.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include "helper.h"
void write_func();
void read_func(char* input);
void getuid_func();
void exit_func();
int main()
{
	struct func_desc fun_desc[4];
	fun_desc[0].func=&write_func;
	//fun_desc[1].func=&read_func;
	fun_desc[2].func=&getuid_func;
	fun_desc[3].func=&exit_func;
	char* input;
	fun_desc[0].func();
	//fun_desc[1].func(input);
	fun_desc[2].func();
	
	return 0;
}


void write_func()
{
const char msg[] = "exit: press 1\nprint id: press 2\nto show folder: 3\nto show time: 4\nprint in to doc:5\n\n\n";
	syscall(4,0, msg, slen(msg));
}

void read_func(char* input)
{
	syscall(3,0,&input,256);
}

void getuid_func()
{
	syscall(24);
}

void exit_func()
{
	syscall(1,0);
}



