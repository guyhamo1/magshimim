#ifndef HELPER_H
#define HELPER_H


 struct func_desc 
{
/* 
   struct to hold function's name and pointer to implementation.
   an array of this type should be constructed in order to properly
   manage 'menu' construct.
*/
    char *name;
    void (*func)(void);
} fun_desc;

int slen(const char * str);
char * __itoa(int num);

#endif
