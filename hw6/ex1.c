#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>


int main()
{
	int fd;
	fd=open("output1.txt",O_WRONLY |O_CREAT, S_IRWXU);
	pid_t pid;
	pid=fork();
	if(pid==-1)
		exit(1);
	if(pid==0)
		while(1)
		{
			write(fd,"im still alive\n",sizeof("im still alive\n"));
			printf("im still alive\n");
		}

	else
	{
		sleep(1);
		write(fd,"Dispayching\n",sizeof("Dispayching\n"));
		printf("Dispayching\n");
		kill(pid,SIGTERM);
		write(fd,"Dispayched\n",sizeof("Dispayched\n"));
		printf("Dispayched\n");
	}
	close(fd);
	return (0);
}
