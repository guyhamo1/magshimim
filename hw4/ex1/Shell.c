#include <dirent.h> 
#include <stdio.h> 
#include <string.h>
#include <stdlib.h>
#include "LineParser.h"


void execute(struct cmdLine* arg);
int main()
{
	char line[2048];
	struct cmdLine* arg;
	struct cmdline* ls;
	ls=parseCmdLines("ls");
	execute(ls);
	fgets(line,2048,stdin);
	while(strcmp(line,"quit")!=0)
	{
		arg=parseCmdLines(line);
		execute(arg);
		execute(ls);
		fgets(line,2048,stdin);
		printf("%s",line);
	}
	freeCmdLines(arg);
	return(0);
}




void execute(struct cmdLine* arg)
{
	pid_t pid;
	pid=fork();
	if(pid==-1)
	{
		perror("fail:");
		exit(1);
	}
	if(pid==0)
	{	
		if(execvp(arg->arguments[0],arg->arguments))
			perror("fail");
	}
	else
	{
		waitpid(pid);
	}
}


