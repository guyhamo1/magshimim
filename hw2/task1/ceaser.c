#include "ceaser.h"
#include <string.h>//
#include <malloc.h>//
char shift_letter(char letter, int offset)
{
	if((letter=='a'||letter=='A')&&offset<0)
	offset=26+offset;
	if((letter=='z'||letter=='Z')&&offset>0)
	offset=26-offset;
	return letter+offset;
}

char * shift_string(char * input, int offset)
{
	int length = strlen(input);//sizeof char*=8 64 or 4 32
	int i;
	char* encrypted=(char*)malloc(sizeof(char)*length);
	for (i = 0; i < length; i++)// ++i
	{
		encrypted[i] = shift_letter(input[i],offset);
	}	
	return encrypted;
}
