#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(int argc,char** argv)
{
	if(argc<2)
	{
		printf("enter a file name");
		exit(1);
	}
	FILE* f=open(argv[1],"r");
	if (f == NULL) 
	{
        	 printf("I couldn't open results.dat for writing.\n");
        	 exit(0);
	}
	int lines=0;
	char line[256];
	while(fgets(line,256, f))
	{
	lines++;
	printf("%s\n",line);
	}
 
	srand(time(NULL));
	int rnd=rand()%lines;
	char com[100];
	sprintf(com,"head -n%d %s|tail -n1",rnd,argv[1]);
	system(com);
	fclose(f);
	
	return(0);
}
