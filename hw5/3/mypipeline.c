#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>


int main()
{
	myPipeLine();
	return (0);
}



void myPipeLine()
{
	
	int pipefd[2];
	pipe(pipefd);
	pid_t pid=fork();
	if(pid==-1)
	{
		perror("fail1");
	}
	if(pid==0)
	{
		close(1);
		dup2(pipefd,0);
		close(0);
		if(execvp("ls","ls -l"))
			perror("fail2");
		
		else
		{
			close(0);
			pid_t pid2=fork();
			if(pid==-1)
			{
				perror("fail3");
			}
			if(pid==0)
			{
				close(0);
				dup2(pipefd,1);
				close(1);
				if(execvp("tail","tail -n 2"))
					perror("fail4");	
			}
				else
				{
					wait(0);
				}
			close(1);
			wait(0);

		}	
		
	}
}
