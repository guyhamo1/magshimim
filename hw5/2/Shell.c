#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "LineParser.h"

void execute(struct cmdLine* arg);
void main()
{
	pid_t pid;
	char line[2048];
	char* end="quit\n";
	struct cmdLine* arg;
	struct cmdline* ls;
	ls=parseCmdLines("ls");
	pid=fork();
	if(pid==-1)
	{
		perror("fail1");
		exit(1);
	}
	if(pid==0)
	{	
		execute(ls);
	}
	else
	{
		wait(0);
	}
	fgets(line,2048,stdin);
	while(strcmp(line,end)!=0)
	{
		arg=parseCmdLines(line);
		pid=fork();
		if(pid==-1)
		{
		perror("fail2");
		exit(1);
		}
		if(pid==0)
		{	
			execute(arg);
		}
		else
		{
		wait(0);
		}
		printf("\n\n");
		pid=fork();
		if(pid==-1)
		{
			perror("fail3");
			exit(1);
		}
		if(pid==0)
		{	
			execute(ls);
		}
		else
		{
			wait(0);
		}
			fgets(line,2048,stdin);
	}
	
	freeCmdLines(arg);
	freeCmdLines(ls);
}




void execute(struct cmdLine* arg)
{	
	if(arg->next)
	execute(arg->next);
	int pipefd[2];
 	int d,d2;
	pipe(pipefd);

 	if(arg->inputRedirect)
 	{
 		close(0);
 		d = open(arg->inputRedirect, S_IROTH);
 		dup2(d,3);
 	}
 	if(arg->outputRedirect)
 	{
 		close(1);
 		d2 = open(arg->outputRedirect, O_WRONLY);
  		dup2(d2,3);
 	}
	if(execvp(arg->arguments[0],arg->arguments))
		perror("fail4");
	exit(0);
}















