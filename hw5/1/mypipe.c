#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

int main()
{
	int pipefd[2];
	char* inbuffer="magshimim";
	char outbuffer[10];
	pid_t pid=0;
	pipe(pipefd);
	pid=fork();
	if(pid==-1)
		perror("fail:");
	else if(pid==0)
	{
		close(pipefd[0]);
		write(pipefd[1],inbuffer,strlen(inbuffer));
		exit(0);
	}
	else
	{
		waitpid(pid);
		close(pipefd[1]);
		read(pipefd[0],outbuffer,sizeof(outbuffer));
		printf("%s\n",outbuffer);
	}
	return(0);
}

